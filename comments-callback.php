<?php 
function koa_comments_callback( $comment, $args, $depth ) {
       $GLOBALS['comment'] = $comment; ?>
                                  
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>" class = 'st-comment-container'> 
                <div class="st-comment-header">
                        <div class="st-comment-avatar st-comment-header-a">
                            <?php echo get_avatar($comment, $size = '60'); ?>
                        </div><!-- avatar , slot-a-->
                        <div class="st-comment-header-b">
                           <span>
                            <span class = 'st-comment-author-name'><?php printf( __( '%s', 'niroon'), get_comment_author_link() ) ?></span>
                             <div class="st-comment-date">
                                  <span title="<?php echo esc_attr($post_date = get_comment_date('F j, Y g:i')); ?>" href="<?php the_permalink(); ?>" class="date"><?php echo esc_attr($post_date = get_comment_date('j F')); ?></span>
                             </div><!-- date -->
                             </span><!-- blocking left -->
                              
                             <div class="st-comment-buttons">
                                    <?php edit_comment_link( __( 'Edit', 'niroon'),'  ','' ) ?>
                                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>  
                          </div><!-- comment edit , comment reply -->
                         </div><!-- commenter info , slot-b -->
                       </div><!-- comment header -->
                         <div class = 'st-comment-content'>
                              <?php comment_text() ?>
                         </div><!-- comment content -->
                        
                             <?php if ( $comment->comment_approved == '0' ) : ?>
                             <em><?php __( 'Your comment is awaiting moderation.', 'niroon' ) ?></em>
                           <?php endif; ?>
            </div><!-- comment container -->
        </li><!-- comment li -->
<?php } ?>